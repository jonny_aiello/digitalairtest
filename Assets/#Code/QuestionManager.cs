﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using U2DProjectCore;

public class QuestionManager : MonoBehaviour {

	// Variables
	public bool useTestQuestions;
	public TestQuestions testQuestions;

	public static List<Question> questionMasterList;
	private List<Question>[] difficultyMasters = new List<Question>[3]; 
	private List<Question>[] availableQuestions = new List<Question>[3]; 
	private int difficulty;
	private bool initialized;

	// Reference Variables
	private RemoteDataManager rdm;

	// Properties
	public int Difficulty { get{return difficulty;} }

// -----------------------------------------------------------------------------
// Unity Events

	// [[ ----- AWAKE ----- ]]
	private void Awake(){
		
		// load difficulty
		difficulty = GameData.LoadDifficulty();

		// init sub-question lists
		// note - won't let me assign new lists to iteratable objects
		// note - never making arrays of lists again, ever
		difficultyMasters[0] = new List<Question>(); 
		difficultyMasters[1] = new List<Question>(); 
		difficultyMasters[2] = new List<Question>(); 
		availableQuestions[0] = new List<Question>();
		availableQuestions[1] = new List<Question>();
		availableQuestions[2] = new List<Question>();

		// init main question list
		// use test questions
		if( useTestQuestions ){
			questionMasterList = new List<Question>();
			testQuestions.GenerateTestQuestions( questionMasterList ); 
			CreateDifficultyLists();

		}else{
			// use remote data	
			rdm = GameObject.Find("DataManager_QuestionsTable")
				.GetComponent<RemoteDataManager>(); 

			if( RemoteDataManager.DataDownloaded ){
				// only need to create the master list once per play session 
				if( questionMasterList == null ){
					questionMasterList = new List<Question>();
					InitializeQuestions( rdm.DLEntries ); 
				}else{ 
					print("questionMasterList already configured"); 
					CreateDifficultyLists();
				}
			}
			else{ 
				Debug.LogError("RemoteDataManager hasn't downloaded questions!"); 
			}
		}
	}

// -----------------------------------------------------------------------------
// Private Methods

	// [[ ----- GET INT ----- ]]
	private int GetInt( string _myArg ){
		return System.Convert.ToInt32( _myArg ); 
	}

	// [[ ----- INITIALIZE QUESTIONS ----- ]]
	private void InitializeQuestions( List<TableEntry> _entries ){
		// zero out list
		if( questionMasterList.Count > 0 ){ questionMasterList.Clear(); }

		// make questions out of entries
		foreach( TableEntry entry in _entries ){
			int id = GetInt(entry.GetValue("id")); 
			string qText = entry.GetValue("questionText"); 
			int cAnswer = GetInt(entry.GetValue("correctAnswer"));

			// set up answer array
			string[] answerSlots = new string[4]{
				entry.GetValue("answer0"),
				entry.GetValue("answer1"),
				entry.GetValue("answer2"),
				entry.GetValue("answer3")
			};
			string[] answers = new string[GetInt(entry.GetValue("answerCount"))];
			for( int i = 0; i < GetInt(entry.GetValue("answerCount")); i++ ){
				answers[i] = answerSlots[i]; 
			}

			// add question to list
			Question newQues = new Question( id, qText, answers, cAnswer );
			questionMasterList.Add( newQues ); 
		}

		// copy master list into available questions
		CreateDifficultyLists();
		print("questions initialized: " + questionMasterList.Count); 
	}

	// [[ ----- CREATE DIFFICULTY LISTS ----- ]]
	private void CreateDifficultyLists(){

		// clear out lists
		for( int i = 0; i < difficultyMasters.Length; i++ ){
			difficultyMasters[i] = new List<Question>();
		}

		// sort and clone
		foreach( Question q in questionMasterList ){
			switch( q.Answers.Length ){
				case 2:
					difficultyMasters[0].Add( q ); 
					break;
				case 3:
					difficultyMasters[1].Add( q );
					break;
				case 4:
					difficultyMasters[2].Add( q );
					break;
				default:
					Debug.Log("switch: value match not found");
					break;
			}
		}

		// set availableQuestions
		SetAvailableQuestions(); 		
	}

	// [[ ----- SET AVAILABLE QUESTIONS ----- ]]
	private void SetAvailableQuestions(){

		if( availableQuestions[difficulty].Count < 2 ){
			// clear list
			availableQuestions[difficulty] = new List<Question>();
			// load current difficulty
			foreach( Question q in difficultyMasters[difficulty] ){
				availableQuestions[difficulty].Add( q ); 
			}
		}	
	}

// -----------------------------------------------------------------------------
// Public Methods
	
	// [[ ----- GET NEXT QUESTION ----- ]]
	public Question GetNextQuestion( Question _currentQ, bool _removeQ ){
		
		// remove current question from list if it was answered correctly
		if( _removeQ ){ availableQuestions[difficulty].Remove(_currentQ); }
		
		// if there are questions left
		if( availableQuestions[difficulty].Count > 1 ){
			// make sure you're returning a different question than the last
			List<Question> diffQuestions = new List<Question>(); 
			foreach( Question q in availableQuestions[difficulty] ){
				if( q.ID != _currentQ.ID ){ diffQuestions.Add(q); }
			}

			return diffQuestions[ Random.Range(0, diffQuestions.Count) ]; 
		}else{
			// if not enough Qs left, reset the list and try again
			CreateDifficultyLists();
			return GetNextQuestion( _currentQ, _removeQ );  
		}
	}

	// [[ ----- GET NEXT QUESTION (overload) ----- ]]
	public Question GetNextQuestion(){
		if( availableQuestions[difficulty].Count > 0 ){
			return availableQuestions[difficulty][
				Random.Range(0, availableQuestions[difficulty].Count)
				];
		}else{ 
			Debug.LogError("GetNextQuestion called before answer list populated");
			return null; 
		}
	}

	// [[ ----- SET DIFFICULTY ----- ]]
	public void SetDifficulty( int _difficulty ){
		difficulty = Mathf.Clamp(_difficulty, 0, 3); 
	}

	// [[ ----- INCREASE DIFFICULTY ----- ]]
	public void IncreaseDifficulty(){
		if( difficulty < 2 ){ 
			difficulty++; 
			SetAvailableQuestions();
		}
	}

	// [[ ----- DECREASE DIFFICULTY ----- ]]
	public void DecreaseDifficulty(){
		if( difficulty > 0 ){ 
			difficulty--; 
			SetAvailableQuestions();
		}
	}
}
