﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StreakGUIManager : MonoBehaviour {

	// Variables
	private List<GameObject> currentXOs = new List<GameObject>(); 

	// Reference Variables
	public Image xImage;
	public Image oImage;
	public RectTransform xoPanel;

	// [[ ----- ADD IMAGE ----- ]]
	private void AddImage( bool _isSuccess ){
		Image imageToUse = null;
		imageToUse = _isSuccess ? oImage : xImage; 

		// instantiate
		GameObject newImage = (GameObject) Instantiate(
			imageToUse.gameObject,
			xoPanel.transform
			);

		currentXOs.Add( newImage ); 
	}

	// [[ ----- UPDATE FAIL ----- ]]
	public void UpdateFail(){ AddImage( false ); }

	// [[ ----- UPDATE SUCCESS ----- ]]
	public void UpdateSuccess(){ AddImage( true ); }

	// [[ ----- CLEAR IMAGES ----- ]]
	public void ClearImages(){
		foreach( GameObject go in currentXOs ){ Destroy(go); }
		currentXOs.Clear();
	}
}
