﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class TestQuestions : MonoBehaviour {

	// [[ ----- GENERATE TEST QUESTIONS ----- ]]
	public void GenerateTestQuestions( List<Question> _masterQList ){
		_masterQList.Add( new Question( 
			0, "this is a sample NORMAL 1", 
			new string[3]{ "a1", "a2", "a3" }, 
			0 
			));
		_masterQList.Add( new Question( 
			1, "this is a sample NORMAL 2", 
			new string[3]{ "a1", "a2", "a3" }, 
			0 
			));
		_masterQList.Add( new Question( 
			2, "this is a sample NORMAL 3", 
			new string[3]{ "a1", "a2", "a3" }, 
			0 
			));

		_masterQList.Add( new Question( 
			3, "this is a sample NORMAL 4", 
			new string[3]{ "a1", "a2", "a3" }, 
			0 
			));
		_masterQList.Add( new Question( 
			4, "this is a sample NORMAL 5", 
			new string[3]{ "a1", "a2", "a3" }, 
			0 
			));
		_masterQList.Add( new Question( 
			5, "this is a sample EASY question 1", 
			new string[2]{ "a1", "a2" }, 
			0 
			));
		_masterQList.Add( new Question( 
			6, "this is a sample EASY question 2", 
			new string[2]{ "a1", "a2" }, 
			0 
			));
		_masterQList.Add( new Question( 
			7, "this is a sample EASY question 3", 
			new string[2]{ "a1", "a2"}, 
			0 
			));
		_masterQList.Add( new Question( 
			8, "this is a sample EASY question 4", 
			new string[2]{ "a1", "a2" }, 
			0 
			));
		_masterQList.Add( new Question( 
			9, "this is a sample EASY question 5", 
			new string[2]{ "a1", "a2" }, 
			0 
			));
		_masterQList.Add( new Question( 
			10, "this is a sample HARD 1", 
			new string[4]{ "a1", "a2", "a3", "a4" }, 
			0 
			));
		_masterQList.Add( new Question( 
			11, "this is a sample HARD 2", 
			new string[4]{ "a1", "a2", "a3", "a4" }, 
			0 
			));
		_masterQList.Add( new Question( 
			12, "this is a sample HARD 3", 
			new string[4]{ "a1", "a2", "a3", "a4" }, 
			0 
			));
		_masterQList.Add( new Question( 
			13, "this is a sample HARD 4", 
			new string[4]{ "a1", "a2", "a3", "a4" }, 
			0 
			));
		_masterQList.Add( new Question( 
			14, "this is a sample HARD 5", 
			new string[4]{ "a1", "a2", "a3", "a4" }, 
			0 
			));
	}
}
