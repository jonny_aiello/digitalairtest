<?php

	// Connection details
	$serverName = "localhost"; 
	$userName = "jonnyTest"; 
	$password = "testPassword"; 
	$dbName = "unity_test_db";
	$tableName = "airquiz_questions"; 

	// Table column names
	$columns = array(
		0 => "id",
		1 => "questionText",
		2 => "correctAnswer",
		3 => "answerCount",
		4 => "answer0",
		5 => "answer1",
		6 => "answer2",
		7 => "answer3",
	);

?>