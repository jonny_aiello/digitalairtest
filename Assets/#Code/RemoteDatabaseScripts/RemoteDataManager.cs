﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace U2DProjectCore {

/**
@brief Downloads database entries from MySQL database  

Contacts retrieve_data.php in order to download all data 
entries from the remote MySQL database. Then stores the data in an array of 
TableEntry container objects. When setting up, the tableColumns array should be
defined in the unity inspector - make sure that it contains one entry for each
column in the MySQL table, and that each column name matches EXACTLY to the way
it appears in tableInfo.php.$columns array. 

Start() contains sample steps for creating a new data entry and then uploading
all outstanding entries to the database. 

_Component Of:_
<br> [GameMaster]

_Class input:_
<br> classes reading from or writing to the MySQL database

@author Jonny Aiello
@version 0.1.1
@date 1/13/17
*/ 
public class RemoteDataManager : MonoBehaviour {

	// Variables
	public bool printOutput; 
	public string phpDownloaderLoc = 
		"http://localhost/unity5testdatabase/retrieveData.php";
	public string phpUploaderLoc = 
		"http://localhost/unity5testdatabase/insertData.php";
	public string phpDeleterLoc = 
		"http://localhost/unity5testdatabase/deleteData.php";
	public string[] tableColumns; 

	private List<TableEntry> downloadedEntries = new List<TableEntry>(); 
	private List<TableEntry> newEntries = new List<TableEntry>(); 
	private bool myDataDownloaded; 
	private bool deleteFinished; 
	private string entryToDelete = "-1"; 

	// universal variables
	private static bool networkTraffic;
	private static bool allDownloadsComplete;
	private static List<RemoteDataManager> allRDMs 
		= new List<RemoteDataManager>(); 

	// Properties
	public List<TableEntry> DLEntries { get{return downloadedEntries;} }
	public List<TableEntry> NewEntries { get{return newEntries;} }
	public bool MyDataDownloaded { get{return myDataDownloaded;} }
	public static bool DataDownloaded { get{return allDownloadsComplete;} }

// -----------------------------------------------------------------------------
// Unity Events

	// [[ ----- START ----- ]]
	void Start(){
		
		// make list of all instances
		allRDMs.Add(this); 

		/*
		// Sample - create/save new data to remote database
		// make new tableEntry
		TableEntry myNewEntry = MakeNewTableEntry(); 
		// populate entry data
		// note: don't need to include 'id' or auto-increment columns
		myNewEntry.LoadValue("playername", "bass"); 
		myNewEntry.LoadValue("score", "3"); 
		// add to upload list
		AddNewEntry( myNewEntry ); 
		// upload
		StartCoroutine("UploadData");
		*/
		// Download remote data on start
		StartCoroutine("DownloadData");  
	}

	// [[ ----- UPDATE ----- ]]
	private void Update(){
		
		// find if all instances have finished downloading
		bool instancesDone = true;
		foreach( RemoteDataManager rdm in allRDMs ){ 
			if( !rdm.MyDataDownloaded ){ instancesDone = false; }
		}
		if( !allDownloadsComplete && instancesDone ){
			allDownloadsComplete = true; 
			print("All Downloads Finished");
		}

		/*
		// Sample - delete specified entry once download completes
		if( myDataDownloaded && !deleteFinished ){
			RemoveEntry( "playername", "bayman" );
			deleteFinished = true; 
		}
		*/
	}

// -----------------------------------------------------------------------------
// Private Methods
	/**@brief parses data from string recieved from php query script */ 
	// [[ ----- PARSE DL STRING ----- ]]
	private string ParseDLString( string _data, string _index ){
		// print("data: " + _data);
		// print("index: " + _index); 
		string value = _data.Substring( _data.IndexOf(_index) + _index.Length ); 
		if( value.Contains("|") ){
			value = value.Remove(value.IndexOf("|")); 
		}
		return value; 
	}

	// [[ ----- PRINT OUTPUT ----- ]]
	private void PrintOutput( string _output, List<TableEntry> _entryList ){
		// print table entries
		foreach( TableEntry te in _entryList ){
			foreach( string column in tableColumns ){
				_output+= column + ":"; 
				_output+= te.GetValue(column) + " "; 
			}
			_output+= "\n";
		}
		print( _output ); 
	}

// -----------------------------------------------------------------------------
// Network Methods

	// [[ ----- DOWNLOAD DATA ----- ]]
	public IEnumerator DownloadData () {
		
		// wait for up/down processes to finish
		while( networkTraffic ){ yield return new WaitForSeconds(0.5f); }
		networkTraffic = true;

		WWW remoteData = new WWW( phpDownloaderLoc );
		
		// wait for the data to finish downloading
		yield return remoteData; 

		// parse downloaded data
		string unparsedString = remoteData.text; 
		string[] lines = unparsedString.Split(';'); 
		if( printOutput ){
			// print(unparsedString);
			// foreach( string line in lines ){ print(line);  }
		}
		
		// populate table entry obj
		downloadedEntries.Clear(); 
			// Split creates a blank entry at end of array
		for( int i = 0; i < lines.Length; i++ ){
			if( lines[i].Length > 0 ){
				TableEntry entry = MakeNewTableEntry(); 
				foreach( string column in tableColumns ){
					string dataValue = ParseDLString(lines[i], column + ":");  
					entry.LoadValue( column, dataValue ); 
				}
				downloadedEntries.Add( entry );  
			}
		}

		// print table entries
		if( printOutput ){
			string output = 
				"Data entries downloaded: ("+downloadedEntries.Count+") \n"; 
			PrintOutput(output, downloadedEntries); 
		}

		myDataDownloaded = true;
		networkTraffic = false; 
	}

	// [[ ----- UPLOAD DATA ----- ]]
	public IEnumerator UploadData(){

		// wait for up/down processes to finish
		while( networkTraffic ){ yield return new WaitForSeconds(0.5f); }
		networkTraffic = true;

		if( newEntries.Count > 0 ){
			// send insert query for each table entry
			for( int i = 0; i < newEntries.Count; i++ ){
				WWWForm form = new WWWForm(); 
				// create post variables
				for( int j = 0; j < tableColumns.Length; j++ ){
					form.AddField( 
						tableColumns[j], 
						newEntries[i].GetValue(tableColumns[j]) 
						);
				}
				// upload
				WWW sentData = new WWW( phpUploaderLoc, form );
				yield return sentData; // wait for the data to finish uploading
				// print( sentData.text ); 
			}
			// print table entries
			if( printOutput ){
				string output = 
					"Data entries uploaded: ("+newEntries.Count+") \n"; 
				PrintOutput(output, newEntries); 
			}
			newEntries.Clear(); 
		} else { Debug.LogWarning("No new entries to upload!"); }
	
		networkTraffic = false; 
	}
	/**@brief deletes specified single entry, requires download entries first */ 
	// [[ ----- DELETE DATA ----- ]]
	public IEnumerator DeleteData(){
		
		// wait for up/down processes to finish
		while( networkTraffic ){ yield return new WaitForSeconds(0.5f); }
		networkTraffic = true;

		// send query
		WWWForm form = new WWWForm();
		form.AddField("id", entryToDelete); 
		WWW deleteData = new WWW( phpDeleterLoc, form );
		yield return deleteData;
		print( deleteData.text );

		networkTraffic = false;

		StartCoroutine("DownloadData");
	}

// -----------------------------------------------------------------------------
// Public Methods

	// [[ ----- MAKE NEW TABLE ENTRY ----- ]]
	public TableEntry MakeNewTableEntry(){return new TableEntry(tableColumns);}

	// [[ ----- ADD NEW ENTRY ----- ]]
	public void AddNewEntry( TableEntry _entry ){ newEntries.Add(_entry); }

	// [[ ----- REMOVE ENTRY ----- ]]
	public void RemoveEntry( string _columnName, string _dataValue ){
		entryToDelete = "-1"; 
		if( downloadedEntries.Count > 0 ){
			// find entry to delete
			foreach( TableEntry te in downloadedEntries ){
				if( te.GetValue(_columnName) == _dataValue ){
					entryToDelete = te.GetValue("id").ToString();
					break;
				}
			}
			if( entryToDelete != "-1" ){ StartCoroutine("DeleteData"); }
			else{ 
				Debug.LogWarning(
					"Match not found in downloadedEntries! " + _dataValue );
			}
			
		} else { Debug.LogWarning("downloadedEntries list empty!"); }
	}
	/**@brief used for checking subsequent downloads after initial start */ 
	// [[ ----- RESET DOWNLOAD STATUS ----- ]]
	public void ResetDownloadStatus(){
		myDataDownloaded = false;
	}
}

/**
@brief Container class for sending and recieving data to/from MySQL server

Class used by RemoteDataManager - contains a dictionary of strings that match
the table columns of a MySQL table specified by RemoteDataManager. 

@author Jonny Aiello
@version 0.1.1
@date 1/13/17
*/ 
public class TableEntry{

	// Variables
	private Dictionary<string, string> valueDict = 
		new Dictionary<string, string>();
	
	// [[ ----- CONSTRUCTOR ----- ]]
	public TableEntry( string[] _columns ){
		foreach( string s in _columns ){ valueDict.Add( s, "" ); }
	}

	// [[ ----- LOAD VALUE ----- ]]
	public void LoadValue( string _column, string _value ){
		if( valueDict.ContainsKey(_column) ){
			valueDict[_column] = _value; 
		} else { Debug.Log("column name not found in valueDict" + _column); }
	}

	// [[ ----- GET ENTRY LENGTH ----- ]]
	public int GetEntryLength(){
		return valueDict.Count; 
	}

	// [[ ----- GET VALUE ----- ]]
	public string GetValue( string _column ){
		return valueDict[_column]; 
	}
}

}