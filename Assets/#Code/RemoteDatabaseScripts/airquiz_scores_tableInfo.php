<?php

	// Connection details
	$serverName = "localhost"; 
	$userName = "jonnyTest"; 
	$password = "testPassword"; 
	$dbName = "unity_test_db";
	$tableName = "airquiz_scores"; 

	// Table column names
	$columns = array(
		0 => "id",
		1 => "timedate",
		2 => "questions_answered",
		3 => "questions_correct",
		4 => "player_won",
	);

?>