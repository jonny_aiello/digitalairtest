<?php

	// http://jonnyaiello.us/unity5testdatabase/airquiz_questions_retrieveData.php

	// Notes: 
	// - update include statement to match file name of 'myTable_tableInfo.php'
	// - insert queries assume that every table has a first column called 'id' 
	// which is set to auto-increment

	// import table info from myTable_tableInfo.php
	include 'airquiz_questions_tableInfo.php'; 

	// make connection
	$conn = new mysqli($serverName, $userName, $password, $dbName);
	
	// check connection
	if(!$conn){
		die( "Connection failed; ". mysqli_connect_error() );
	}

	// query
	$sqlQuery = "SELECT "; 
	for ($i=0; $i < count($columns); $i++) { 
		$sqlQuery = $sqlQuery . $columns[$i];
		if ($i < (count($columns)-1) ) {
			$sqlQuery = $sqlQuery . ", ";
		} else{ $sqlQuery = $sqlQuery . " "; }
	}
	$sqlQuery = $sqlQuery . "FROM " . $tableName; 
	// send query 
	$result = mysqli_query($conn, $sqlQuery); 
	// check query
	if(!$result){
		die( "Query failed; ". mysqli_connect_error() );
	}

	// manage output
	if (mysqli_num_rows($result) > 0 ) {
		while($row = mysqli_fetch_assoc($result)){
			$outputString = ""; 
			foreach( $columns as $value ){
				$outputString = $outputString . $value . ":" . $row[$value] . "|"; 
			}
			$outputString = $outputString . ";"; 
			echo( $outputString );
		}
	}
?>