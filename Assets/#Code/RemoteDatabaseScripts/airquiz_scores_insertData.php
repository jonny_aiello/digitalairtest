<?php

	// http://jonnyaiello.us/unity5testdatabase/airquiz_scores_insertData.php

	// Notes: 
	// - update include statement to match file name of 'myTable_tableInfo.php'
	// - insert queries assume that every table has a first column called 'id' 
	// which is set to auto-increment

	// import table info from myTable_tableInfo.php
	include 'airquiz_scores_tableInfo.php'; 

	// make connection
	$conn = new mysqli($serverName, $userName, $password, $dbName);
	
	// check connection
	if(!$conn){
		die( "Connection failed.". mysqli_connect_error() );
	}

	// insert query
	$sqlQuery = "INSERT INTO `".$tableName."` ("; 
	for ($i=0; $i < count($columns); $i++) {
		$sqlQuery = $sqlQuery."`".$columns[$i]."`";
		if ($i < (count($columns)-1) ) { $sqlQuery = $sqlQuery . ", "; }
			// don't comma last item
	}
	$sqlQuery = $sqlQuery.") VALUES (NULL, "; 
	for ($j=1; $j < count($columns); $j++) {
		$sqlQuery = $sqlQuery . "'" . $_POST[$columns[$j]] . "'";
		if ($j < (count($columns)-1) ) { $sqlQuery = $sqlQuery . ", "; }
			// don't comma last item 
	}
	$sqlQuery = $sqlQuery . ");";

	echo( $sqlQuery . " - " ); 

	$result = mysqli_query($conn, $sqlQuery); 

	if (!$result) {
		echo( "ERROR" );
	}
	else{ echo("SUCCESS"); }

?>