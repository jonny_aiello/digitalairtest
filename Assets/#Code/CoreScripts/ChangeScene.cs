﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace U2DProjectCore { 
	/**
	@brief Transitions from start.scene / end.scene on button prompt 

	Part of Project Core unity package. 
	Triggers the next scene once the specified user input is detected

	@author Jonny Aiello
	@version 0.1.1
	@date 3/23/16
	*/ 
	public class ChangeScene : MonoBehaviour {

		// Variables
		public string triggerInput = "Fire1";
		public string nextLevelName = "Level01";
		public bool clickToContinue; 
		private bool inputLock;

		// [[ ----- UPDATE ----- ]]
		void Update () {
			if(Input.GetButtonDown(triggerInput) && !inputLock && clickToContinue){
				inputLock = true;
				SceneManager.LoadScene(nextLevelName); 
			} 
		}

		// [[ ----- NEXT SCENE ----- ]]
		public void NextScene(){
			if( !inputLock && RemoteDataManager.DataDownloaded ){
				inputLock = true;
				SceneManager.LoadScene(nextLevelName);
			}
		}
	}
}