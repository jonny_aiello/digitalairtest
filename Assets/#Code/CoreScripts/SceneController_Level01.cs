﻿using UnityEngine;
using System.Collections;

namespace U2DProjectCore { 

	/**
	@brief Manages timing of scene-based logic 

	Part of Project Core unity package.
	This script can serve a number of functions related to scene-local logic and 
	timing. For example, this is where you would place code needed to initialize
	or synchronize other scripts at the level's start.  This is also a good location
	to determine the conditions for loading the next level. 

	@author Jonny Aiello
	@version 0.1.1
	@date 3/29/16
	*/ 
	public class SceneController_Level01 : MonoBehaviour {

		// Variables
		private TimeController timeController; 
		public Question testQ;

		// Reference Variables
		private GameObject gameMaster;

	// -----------------------------------------------------------------------------
	// Unity Events

		private void Awake(){
			// sample make question logic
			int id = 0;
			string qText = "This is a test question, the second answer is the correct answer";
			string[] testAns = new string[3]{ "first answer", "second answer", "third answer" };
			int correctA = 1; 
			testQ = new Question( id, qText, testAns, correctA );

			// print( testQ.QuestionText ); 
		}

		// [[ ----- START ----- ]]
		void Start () {

			// init references
			if( GameObject.Find("[GameMaster]") ){
				gameMaster = GameObject.Find("[GameMaster]"); 
			} else{ Debug.LogWarning("Could not find GameMaster obj!" ); }
			if( gameMaster.GetComponent<TimeController>() ){
				timeController = gameMaster.GetComponent<TimeController>(); 
			} else{ Debug.LogWarning("Could not find TimeController component!" ); }

			// init scene
			if( Time.timeScale != 1 ){ timeController.SetTimeNormal(); }
			
		}
		
		// [[ ----- UPDATE ----- ]]
		void Update () {
		
		}

	// -----------------------------------------------------------------------------
	// Public Methods

		
	}
}