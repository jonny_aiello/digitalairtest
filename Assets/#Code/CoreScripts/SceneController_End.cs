﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace U2DProjectCore { 

	/**
	@brief Manages timing of scene-based logic 

	Part of Project Core unity package.
	This script can serve a number of functions related to scene-local logic and 
	timing. For example, this is where you would place code needed to initialize
	or synchronize other scripts at the level's start.  This is also a good location
	to determine the conditions for loading the next level. 

	@author Jonny Aiello
	@version 0.1.1
	@date 3/29/16
	*/ 
	public class SceneController_End : MonoBehaviour {
 
		// Variables
		public int maxDisplayEntries; 
		private bool scoresPopulated; 
		private string[] yesNoString = new string[2]{ "No", "Yes" }; 

		// Reference Variables
		public Text headerText;
		public Text finalScore;
		public RectTransform recentScoresPanel;
		public RectTransform textPrefab;
		
		private ColorSetter headerColorSetter;
		private GameObject gameMaster;
		private RemoteDataManager scoreRDM;

	// -----------------------------------------------------------------------------
	// Unity Events
		
		// [[ ----- START ----- ]]
		void Start () {

			// init references
			if( GameObject.Find("[GameMaster]") ){
				gameMaster = GameObject.Find("[GameMaster]"); 
			} else{ Debug.LogWarning("Could not find GameMaster obj!" ); }

			scoreRDM = gameMaster.transform.Find("DataManager_ScoresTable").
				GetComponent<RemoteDataManager>(); 
			headerColorSetter = headerText.GetComponent<ColorSetter>();

			// reset download status for score RDM
			scoreRDM.ResetDownloadStatus();

			// set end game details
			if( GameData.PlayerWon ){
				headerText.text = "WELL DONE! YOU WIN!";
				headerColorSetter.SetGreen();
			}else{ 
				headerText.text = "SORRY, YOU LOSE!"; 
				headerColorSetter.SetRed();
			}
			finalScore.text = "Correct Answers: " + GameData.CorrectQuestions
				+ "/" + (GameData.TotalQuestions + 1); 

			// Save game difficulty
			GameData.SaveData();

			// Save play record to cloud
			UpdateScoreList();

		}

		// [[ ----- UPDATE ----- ]]
		void Update () {

			// populate table once entries are downloaded
			if( !scoresPopulated && scoreRDM.MyDataDownloaded ){
				scoresPopulated = true; 
				PopulateScoreTable(); 
			}
		}

	// -----------------------------------------------------------------------------
	// Private Methods

		// [[ ----- UPDATE SCORE LIST ----- ]]
		private void UpdateScoreList(){

			// make time stamp
			int year = System.DateTime.Now.Year;
			int month = System.DateTime.Now.Month;
			int day = System.DateTime.Now.Day;
			int hour = System.DateTime.Now.Hour;
			int minute = System.DateTime.Now.Minute;
			string dateText = string.Format( 
					"{0}/{1}/{2} {3}:{4}", 
					month, day, year, hour, minute 
					);

			// make table entry
			TableEntry newScoreEntry = scoreRDM.MakeNewTableEntry(); 
			newScoreEntry.LoadValue( "timedate", dateText );
			newScoreEntry.LoadValue( 
				"questions_answered", (GameData.TotalQuestions+1).ToString()
				);
			newScoreEntry.LoadValue( 
				"questions_correct", GameData.CorrectQuestions.ToString()
				);
			int winningInt = System.Convert.ToInt32( GameData.PlayerWon ); 
			newScoreEntry.LoadValue(
				"player_won", winningInt.ToString() 
				);

			// save and upload entry
			scoreRDM.AddNewEntry( newScoreEntry ); 
			scoreRDM.StartCoroutine("UploadData");

			// download entries
			scoreRDM.StartCoroutine("DownloadData");
		}

		// [[ ----- POPULATE SCORE TABLE ----- ]]
		private void PopulateScoreTable(){

			List<TableEntry> dlEntries = scoreRDM.DLEntries; 

			// protect if entries list is empty
			if( dlEntries.Count == 0 ){ 
				Debug.LogWarning("No play record entries!");

			}else{
				// determine max records to display
				int displayNum = 0; 
				if( dlEntries.Count > maxDisplayEntries ){
					displayNum = maxDisplayEntries;
				}else{ displayNum = dlEntries.Count; }

				// make string (newest entries first)
				int listBottom = dlEntries.Count - 1;
				int stopPoint = dlEntries.Count - (1 + displayNum);
				for( int i = listBottom; i > stopPoint; i-- ){
					// set yesno for player won
					int yesNoInt = System.Convert.ToInt32(
						dlEntries[i].GetValue("player_won")
						);
					string yesNo = yesNoString[ yesNoInt ];
					// set values
					string playText =
						string.Format(
							"Date: {0} Correct Answers: {1}/{2} Won: {3}",
							dlEntries[i].GetValue("timedate"),
							dlEntries[i].GetValue("questions_correct"),
							dlEntries[i].GetValue("questions_answered"),
							yesNo
							);
					AddPlayToList( playText ); 
				}
			}
		}

		// [[ ----- ADD PLAY TO LIST ----- ]]
		private void AddPlayToList( string _playText ){
			// instantiate
			GameObject playRecord = (GameObject) Instantiate(
				textPrefab.gameObject,
				recentScoresPanel.transform
				);
			playRecord.GetComponent<Text>().text = _playText; 
		}
		
	}
}