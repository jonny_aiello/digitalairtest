﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ColorSetter : MonoBehaviour {

	// Variables
	public Color green;
	public Color red; 
	public bool hasText;
	public bool hasImage;
	private Text colorText;
	private Image colorImage;

	// [[ ----- START ----- ]]
	void Start () {
		// get component
		if( hasText ){
			colorText = GetComponent<Text>();
		}
		if( hasImage ){
			colorImage = GetComponent<Image>();
		}
	}

	// [[ ----- SET GREEN ----- ]]
	public void SetGreen(){
		if( hasText ){ colorText.color = green; }
		if( hasImage ){ colorImage.color = green; }
	}

	// [[ ----- SET RED ----- ]]
	public void SetRed(){
		if( hasText ){ colorText.color = red; }
		if( hasImage ){ colorImage.color = red; }
	}
}
