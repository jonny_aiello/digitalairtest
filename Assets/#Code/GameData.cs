﻿using UnityEngine;
using System.Collections;
/**
@brief Holds persistant and single-playthrough data 

Keeps track of the user's play data to display during the game over screen.
Also manages the difficulty rating, which is saved on game over and loaded each
time the scene begins. 

_Component of:_
<br> [GameMaster]

_Class input:_
<br> Classes that reference or change gameplay stats

@author Jonny Aiello
@version 0.0.0
@date 0/0/00
*/ 
public class GameData : MonoBehaviour {

	// Variables
	private static int totalQuestions;
	private static int correctQuestions;
	private static int gameDifficulty; 
	private static bool playerWon;

	// Properties
	public static int TotalQuestions { get{return totalQuestions;} }
	public static int CorrectQuestions { get{return correctQuestions;} }
	public static int GameDifficulty { get{return gameDifficulty;} }
	public static bool PlayerWon { get{return playerWon;} }

	// [[ ----- SET GAME STATS ----- ]]
	public static void SetGameStats( 
		int _total, int _correct, int _difficulty, bool _won 
		){

		totalQuestions = _total;
		correctQuestions = _correct;
		gameDifficulty = _difficulty; 
		playerWon = _won; 
	}

	// [[ ----- SAVE DATA ----- ]]
	public static void SaveData(){
		PlayerPrefs.SetInt("difficulty", GameDifficulty); 
	}

	// [[ ----- LOAD DIFFICULTY ----- ]]
	public static int LoadDifficulty(){
		return PlayerPrefs.GetInt("difficulty");
	}
}
