﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeaderScript : MonoBehaviour {

	// Variables
	private string[] startLevel = new string[3];
	private string[] setHarder = new string[3];
	private string[] setSofter = new string[3];
	private string[] nearWin = new string[3];
	private string[] nearLose = new string[3];
	private string[] normal = new string[3];

	// Reference Variables
	public Text headerText; 

	// [[ ----- AWAKE ----- ]]
	private void Awake(){
		// init quotes

		startLevel[0] = "Ok, here we go...";
		startLevel[1] = "Are you ready?"; 
		startLevel[2] = "Let's do this!"; 

		setHarder[0] = "Now things get tricky...";
		setHarder[1] = "Those questions were too easy for you."; 
		setHarder[2] = "A tough-guy, huh?";

		setSofter[0] = "Ok, here's an easy one";
		setSofter[1] = "I know you know this";
		setSofter[2] = "I guess I over-estimated you..."; 

		nearWin[0] = "One more to go!";
		nearWin[1] = "Almost there!"; 
		nearWin[2] = "Game point!!";

		nearLose[0] = "Three strikes and you're out..."; 
		nearLose[1] = "Not like this..."; 
		nearLose[2] = "Game over, man, game over!"; 

		normal[0] = "Tick tock, tick tock.";
		normal[1] = "Hmmmmmmmmmmmm";
		normal[2] = "Our chess game continues..."; 
	}

	// [[ ----- SET HEADER ----- ]]
	private void SetHeader( string[] _quotes ){
		int index = Random.Range(0, 3); 
		headerText.text = _quotes[index]; 
	}

	// [[ ----- START LEVEL ----- ]]
	public void StartLevel(){ SetHeader( startLevel ); }

	// [[ ----- SET HARDER ----- ]]
	public void SetHarder(){ SetHeader( setHarder ); }

	// [[ ----- SET SOFTER ----- ]]
	public void SetSofter(){ SetHeader( setSofter ); }

	// [[ ----- NEAR WIN ----- ]]
	public void NearWin(){ SetHeader( nearWin ); }

	// [[ ----- NEAR LOSE ----- ]]
	public void NearLose(){ SetHeader( nearLose ); }

	// [[ ----- NORMAL QUOTE ----- ]]
	public void NormalQuote(){ SetHeader( normal ); }

}
