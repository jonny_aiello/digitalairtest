﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnswerButton : MonoBehaviour {

	// Variables
	private AnswerButtonController abc; 
	private int buttonIndex;

	// [[ ----- INITIALIZE BUTTON ----- ]]
	public void InitializeButton( int _index, AnswerButtonController _abc ){
		buttonIndex = _index;
		abc = _abc;
		
		// bind delegate
		GetComponent<Button>().onClick.AddListener( 
			() => { SendSelection(); } 
		);
	} 

	// [[ ----- SEND SELECTION ----- ]]
	private void SendSelection(){
		abc.GetSelection( buttonIndex ); 
	}
}
