﻿using UnityEngine;
using System.Collections;

public class Question {

	// Variables
	private int id;
	private string questionText; 
	private string[] answers;
	private int correctAnswer; 

	// Properties
	public int ID { get{return id;} }
	public string QuestionText { get{return questionText;} }
	public string[] Answers { get{return answers;} }
	public int CorrectAnswer { get{return correctAnswer;} }

	public Question( int _id, string _qText, string[] _answers, int _correctA ){
		id = _id;
		questionText = _qText;
		answers = _answers;
		correctAnswer = _correctA; 
	}
}
