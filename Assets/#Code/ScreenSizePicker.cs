﻿using UnityEngine;
using UnityEngine.UI; 
using System.Collections;

public class ScreenSizePicker : MonoBehaviour {

	// Variables
	public Vector2[] resolutions;

	// Reference Variables
	public RectTransform buttonWindow;
	public RectTransform resolutionButton;
	 
	// [[ ----- START ----- ]]
	void Start () {
		GenerateButtons();
	}

	// [[ ----- GENERATE BUTTONS ----- ]]
	private void GenerateButtons(){
		for( int i = 0; i < resolutions.Length; i++ ){
			
			// instantiate
			GameObject buttonClone = (GameObject) Instantiate(
				resolutionButton.gameObject,
				buttonWindow.transform
				);
			buttonClone.transform.SetAsFirstSibling();
			
			// set details
			Button newButton = 
				buttonClone.transform.Find("Rez_Button").GetComponent<Button>(); 
			string buttonLabel = 
				resolutions[i].x.ToString()+ " x " +resolutions[i].y.ToString(); 
			Text bText = newButton.transform.Find("Text").GetComponent<Text>();
			bText.text = buttonLabel; 

			// set reference and index
			ScreenRezButton srb = newButton.GetComponent<ScreenRezButton>(); 
			srb.InitializeButton( i, this ); 

		}
	}
	/**@brief called from generated buttons */ 
	// [[ ----- SET SCREEN SIZE ----- ]]
	public void SetScreenSize( int _index ){
		Screen.SetResolution(
			(int)resolutions[_index].x, 
			(int)resolutions[_index].y, 
			false
			);
		Debug.Log("ScreenSize Set: " 
			+ resolutions[_index].x.ToString() 
			+ resolutions[_index].y.ToString()
			);
	}
}
