﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using U2DProjectCore;

public class Director : MonoBehaviour {

	// Variables
	private Question currentQuestion;
	private int currentState;
	private int numAskedQuestions;
	private int numCorrectQuestions; 
	private int winStreak; 
	private bool questionPassed;
	private bool gameOver; 
	private bool timerFailLock;

	// States
	private const int questionState = 0; 
	private const int failState = 1; 
	private const int successState = 2; 
	private const int gameOverState = 3; 

	// Reference Variables
	public Text questionText;
	public RectTransform contentPanel;
	public RectTransform xImage;
	public RectTransform oImage;
	private SceneController_Level01 sc;
	private AnswerButtonController abc;
	private QuestionManager qm;
	private TimerScript ts; 
	private HeaderScript headerScript;
	private StreakGUIManager streakManager;
	private ColorSetter barColorSetter;

// -----------------------------------------------------------------------------
// Unity Events

	// [[ ----- START ----- ]]
	void Start () {
		try{
			sc = GetComponent<SceneController_Level01>();
			abc = GetComponent<AnswerButtonController>(); 
			qm = GetComponent<QuestionManager>(); 
			ts = GetComponent<TimerScript>(); 
			headerScript = GetComponent<HeaderScript>(); 
			streakManager = GetComponent<StreakGUIManager>();
			barColorSetter = GameObject.Find("TimerBar_Image")
				.GetComponent<ColorSetter>(); 
		}catch{ Debug.LogWarning("Couldn't find referenced scripts!"); }

		// set up first question
		currentQuestion = qm.GetNextQuestion();
		questionText.text = currentQuestion.QuestionText; 
		abc.SetUpAnswers(currentQuestion.Answers, currentQuestion.CorrectAnswer);
		RefresheaderScriptcreen();

		// set up GUI elements
		headerScript.StartLevel();
		streakManager.ClearImages();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

// -----------------------------------------------------------------------------
// Private Methods

	// [[ ----- LOAD NEXT QUESTION ----- ]]
	private void LoadNextQuestion(){

		ClearScreen();
		
		// load next question	
		currentQuestion = qm.GetNextQuestion( currentQuestion, questionPassed ); 
		questionText.text = currentQuestion.QuestionText; 
		abc.SetUpAnswers(currentQuestion.Answers, currentQuestion.CorrectAnswer);
		numAskedQuestions++; 
		
		DrawScreen();
	}

	// [[ ----- REFRESH SCREEN ----- ]]
	private void RefresheaderScriptcreen(){
		ClearScreen();
		DrawScreen();
	}

	// [[ ----- CLEAR SCREEN ----- ]]
	private void ClearScreen(){
		contentPanel.gameObject.SetActive(false);
		xImage.gameObject.SetActive(false);
		oImage.gameObject.SetActive(false);
	}
	/**@brief fail/success states automatically reload question using invoke */ 
	// [[ ----- DRAW SCREEN ----- ]]
	private void DrawScreen(){
		switch( currentState ){
			case questionState:
				SetTimer();
				contentPanel.gameObject.SetActive(true);
				break;
			case failState:
				xImage.gameObject.SetActive(true);
				ShowResultScreen();
				break;
			case successState:
				oImage.gameObject.SetActive(true);
				ShowResultScreen();
				break;
			case gameOverState:
				SceneManager.LoadScene("End");
				break;
			default:
				Debug.Log("switch: value match not found");
				break;
		}
	}

	// [[ ----- SHOW RESULT SCREEN ----- ]]
	private void ShowResultScreen(){
		if( !gameOver ){ currentState = questionState; }
		else{ currentState = gameOverState; }
		Invoke( "RefresheaderScriptcreen", 1f );
	}

	// [[ ----- SET TIMER ----- ]]
	private void SetTimer(){
		switch( qm.Difficulty ){
			case 0:
				ts.ResetTimer( 20f );
				break;
			case 1:
				ts.ResetTimer( 15f ); 
				break;
			case 2:
				ts.ResetTimer( 10f ); 
				break;
			default:
				Debug.Log("switch: value match not found");
				break;
		}
		 
		ts.StartTimer();
		timerFailLock = false;
	}

	// [[ ----- UPDATE WIN STREAK ----- ]]
	private void UpdateWinStreak(){
		
		headerScript.NormalQuote();

		// reset
		if( winStreak <= 0 ){ 
			winStreak = 0;
			streakManager.ClearImages(); 
			barColorSetter.SetGreen();
		}
		winStreak++;
		streakManager.UpdateSuccess();

		// difficulty
		if( winStreak == 2 ){ 
			qm.IncreaseDifficulty(); 
			headerScript.SetHarder();
		} else if( winStreak == 4 ){
			qm.IncreaseDifficulty();
			headerScript.NearWin();
		}

		// win condition
		if( winStreak > 4 ){
			gameOver = true;  
			GameData.SetGameStats(
				numAskedQuestions, 
				numCorrectQuestions, 
				qm.Difficulty,
				true);
		}

		print("winStreak: " + winStreak);
	}

	// [[ ----- UPDATE LOSE STREAK ----- ]]
	private void UpdateLoseStreak(){
		
		headerScript.NormalQuote();
		
		// reset
		if( winStreak >= 0 ){ 
			winStreak = 0; 
			streakManager.ClearImages();
			headerScript.SetSofter();
			barColorSetter.SetRed(); 
		}
		winStreak--;
		streakManager.UpdateFail();

		// difficulty
		if( winStreak == -2 ){ 
			qm.DecreaseDifficulty(); 
			headerScript.NearLose();
		}

		// lose condition
		if( winStreak < -2 ){
			gameOver = true; 
			GameData.SetGameStats(
				numAskedQuestions, 
				numCorrectQuestions, 
				qm.Difficulty,
				false);
			
		}

		print("winStreak: " + winStreak);
	}

// -----------------------------------------------------------------------------
// Public Methods
	
	// [[ ----- PASS QUESTION ----- ]]
	public void PassQuestion(){
		currentState = successState;
		questionPassed = true; 
		timerFailLock = true;

		numCorrectQuestions++;
		UpdateWinStreak();

		LoadNextQuestion();
	}

	// [[ ----- FAIL QUESTION ----- ]]
	public void FailQuestion(){
		// makes sure the timer doesn't fail question during success result
		if( !timerFailLock ){
			currentState = failState; 
			questionPassed = false; 
			timerFailLock = true;

			UpdateLoseStreak();

			LoadNextQuestion();
		}
	}
}
