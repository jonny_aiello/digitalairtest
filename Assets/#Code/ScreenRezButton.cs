﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenRezButton : MonoBehaviour {

	// Variables
	private ScreenSizePicker ssp; 
	private int buttonIndex;

	// [[ ----- INITIALIZE BUTTON ----- ]]
	public void InitializeButton( int _index, ScreenSizePicker _ssp ){
		buttonIndex = _index;
		ssp = _ssp;
		
		// bind delegate
		GetComponent<Button>().onClick.AddListener( 
			() => { SendSelection(); } 
		);
	} 

	// [[ ----- SEND SELECTION ----- ]]
	private void SendSelection(){
		Debug.Log("Button clicked: " + buttonIndex);
		ssp.SetScreenSize( buttonIndex ); 
	}
}
