﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic; 

public class AnswerButtonController : MonoBehaviour {

	// Variables
	// private int buttonCount; 
	private List<GameObject> currentButtons = new List<GameObject>(); 
	private int correctAns;

	// Reference Variables
	public RectTransform answerPanel; 
	public RectTransform answerButton; 
	private Director director; 

	// Use this for initialization
	void Start () {
		director = GetComponent<Director>(); 
	}
	
	// [[ ----- CLEAR BUTTONS ----- ]]
	public void ClearButtons(){
		foreach( GameObject button in currentButtons ){ Destroy(button); }
		currentButtons.Clear();
	}

	// [[ ----- SET UP ANSWERS ----- ]]
	public void SetUpAnswers( string[] _answers, int _correctAns ){
		// set correct answer
		correctAns = _correctAns; 
		
		// make buttons
		ClearButtons();
		for( int i = 0; i < _answers.Length; i++ ){
			// determine random order
			bool changeOrder = false; 
			if( i > 0 ){ changeOrder = (Random.value > 0.5f); }
			GenerateButton( _answers[i], i, changeOrder ); 
		}
	}

	// [[ ----- GENERATE BUTTON ----- ]]
	private void GenerateButton( string _bText, int _bIndex, bool _changeOrder ){
		
		// instantiate
		GameObject buttonClone = (GameObject) Instantiate(
			answerButton.gameObject,
			answerPanel.transform
			);

		// determine random order
		if( _changeOrder ){ buttonClone.transform.SetAsFirstSibling(); }
		
		// add to list
		currentButtons.Add( buttonClone ); 
		
		// set details
		Button newButton = buttonClone.GetComponent<Button>();  
		Text bText = newButton.transform.Find("Text").GetComponent<Text>();
		bText.text = _bText; 

		// set reference and index
		AnswerButton buttonScript = newButton.GetComponent<AnswerButton>(); 
		buttonScript.InitializeButton( _bIndex, this ); 
		// buttonCount++;
	}

	// [[ ----- GET SELECTION ----- ]]
	public void GetSelection( int _index ){
		if( _index == correctAns ){ director.PassQuestion(); }
		else{ director.FailQuestion(); }
	}
}
